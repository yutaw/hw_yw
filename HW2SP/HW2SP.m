%% Data
%This is a code for HW2SP by Yuta Watabe

%Set parameters
B = 5; %Number of grids for p_t
N = 201; %Number of grids for k
tol = 1e-6; %Tolerance factor

delta = 0.95; %Discount factor
rho = 0.5; %Persistent parameter
p0 = 0.5;
sigmau = 0.1;
%% Problem 1.2
%Make grids that approximates process for p_t with 21 grids points
[pi,p,invdist] = tauchen(B,p0,rho,sigmau);

%% Problem 1.3

%Set state space for k
kl = 0;
kh = 100;
k = linspace(kl,kh,N);

%Setting per-period utility
Util = -1000*ones(N,B,N); %Per-period utility function. Punish if they buy lumber
for i = 1:N;
    for j = 1:B;
      for l = 1:i;
         Util(i,j,l)=p(j)*(k(i)-k(l))-0.2*(k(i)-k(l)).^(1.5);
      end
   end
end

% VFI
V = zeros(N,B);
t = 1;

while t > tol
    Vold = V;
    EV= pi*V';
    EV = permute(EV',[3,2,1]);
    EVr = repmat(EV,N,1,1);
    Vc = Util + delta*EVr;
    V = max(Vc,[],3);
    t = abs(max(max(V-Vold)));
end

figure(1);
if B == 21;
    plot(k,V(:,8),k,V(:,11),k,V(:,14)); %8th variable correspond to 0.9
elseif B == 5;
    plot(k,V(:,2),k,V(:,3),k,V(:,4));
end
legend('p=0.9','p=1','p=1.1');
title('Figure 1');

%% Problem 1.4
%Calculating policy function
[Junk,policy] = max(Vc,[],3);
knext = k(policy);

figure(2);
plot(p,knext(1:20:201,:));
title('Figure 2');

%% Problem 1.5

% Simulate price
CDF = cumsum(pi');
CDF = CDF';

T= 20;
S= 100;
if B == 21;
    psim = ones(S,T)*11;
elseif B == 5;
    psim = ones(S,T)*3;
end
rng(1);
shock = rand(S,T);

for s = 1:S
    for t = 1:T-1
        for b = 1:B
            if shock(s,t+1) < CDF(psim(s,t),b); 
            psim(s,t+1) = b;
            break
            end
        end
    end
end

policysim = ones(S,1)*201;
ksim = ones(S,1)*100;
for s = 1:S
    for t = 1:T-1
        policysim(s,t+1) = policy(policysim(s,t),psim(s,t));
        ksim(s,t+1) = k(policysim(s,t+1));
    end
end

figure(3)
graphY = [mean(ksim,1);prctile(ksim,95,1);prctile(ksim,6,1)];
graphX = 1:20;
plot(graphX,graphY(1,:),graphX,graphY(2,:),graphX,graphY(3,:));
legend('mean','95th percentile','5th percentile');
title('Figure 3')