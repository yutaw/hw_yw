%% This is HW3, written by Yuta Watabe

% I will display the result in last section (except for the eigenvalues).

%% Setting

%Set diary
diary('HW3YutaWatabe.txt');

% Load data
load('hw3.mat')

% Setting parameters. Maximization options follow default of fminunc.
param.itermax = 1000;
param.tol = 1e-6;
[param.nsample, param.nbeta] = size(X);
param.X =X;
param.y =y;

% Set initial value
beta0 = zeros(param.nbeta,1);

% Set functions
loglik = @(beta)likeli(beta,param);
loglikder = @(beta)likelider(beta,param);
resid = @(beta)NLLS(beta,param);

%% Question 1

% Fminunc without derivatives
options=optimoptions('fminunc','GradObj','off','Algorithm','quasi-newton','HessUpdate','bfgs');
[beta.alg1,fval.alg1,exitflag.alg1,output.alg1]=fminunc(loglik,beta0,options);
iterations.alg1 = output.alg1.iterations;
funcCount.alg1 = output.alg1.funcCount;

% Fminunc with derivatives
options = optimoptions('fminunc','GradObj','on','Algorithm','quasi-newton','HessUpdate','bfgs');
[beta.alg2,fval.alg2,exitflag.alg2,output.alg2] = fminunc(loglikder,beta0,options);
iterations.alg2 = output.alg2.iterations;
funcCount.alg2 = output.alg2.funcCount;

% Nelder-Mead 
[beta.alg3,fval.alg3,exitflag.alg3,output.alg3]=fminsearch(loglik,beta0);
% Here default 'MAxFunEvals' parameter fails, you need to run algo for
% longer number of iterations. see answer key
iterations.alg3 = output.alg3.iterations;
funcCount.alg3 = output.alg3.funcCount;

% BHHH
beta4 = beta0;
[B0, g0] = BHHH(beta4,param);

for it=1:param.itermax
[B, grad] = BHHH(beta4,param);
beta4 = beta4 + inv(B)*grad';
if max(abs(grad))<param.tol
    display('Gradient close to zero')
    beta.alg4 = beta4;
    fval.alg4 = grad;
    exitflag.alg4 = 1;
    iterations.alg4 =it;
    funcCount.alg4 =it;
    break
end
end

if exitflag.alg4 ~= 1
        display('Objective function lost')
        beta.alg4 = NaN;
        fval.alg4 = NaN;
        exitflag.alg4 = 0;
end


%% Question 2
% Eigenvalue of initial Hessian approximation is 
eigenB0 = eig(B0)
% Eigenvalue of parameters estimated Hessian approximation is 
eigenB = eig(B)

%% Question 3
beta5 = beta0;

for it = 1:param.itermax
[residual,jacob,grad,RSS] = resid(beta5);
beta5 = beta5 - inv(jacob' * jacob) * jacob' * residual;
if max(abs(grad))<param.tol
    display('Gradient close to zero')
    beta.alg5 = beta5;
    fval.alg5 = grad;
    exitflag.alg5 = 1;
    iterations.alg5 =it;
    funcCount.alg5 =it;
    break
end
end

if exitflag.alg5 ~= 1
        display('Objective function lost')
        beta.alg5 = NaN;
        fval.alg5 = NaN;
        exitflag.alg5 = 0;
end

%% Display result

betaresult =struct2table(beta)
iterationresult = struct2table(iterations)
funcCountresult = struct2table(funcCount)

diary('off');
