%This is a likelihood function
function[likelihood]= likeli(beta,p)
xb = p.X*beta;
yfac = factorial(p.y);
like = -exp(xb) + p.y.*xb - log(yfac);
likelihood = -1*sum(like);
end