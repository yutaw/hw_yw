%This function calculates the residual
function[residual,jacob,grad,RSS]= NLLS(beta,p)
xb = p.X*beta;
temp = repmat(-exp(xb),1,p.nbeta);
residual =p.y - exp(xb);
jacob = temp .* p.X;
grad = jacob' * residual;
RSS = sum(residual.^2);
end
