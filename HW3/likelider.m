%This is a likelihood function and gradient
function[likelihood,gradient]= likeli(beta,p)
xb = p.X*beta;
yx = p.y'*p.X;
yfac = factorial(p.y);
like = -exp(xb) + p.y.*xb - log(yfac);
likelihood = -1*sum(like);
gradient = -(-exp(xb)'*p.X + yx);
end