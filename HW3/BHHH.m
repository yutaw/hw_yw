%This function calculates the BHHH components
function[B,g]= BHHH(beta,p)
temp = -exp(p.X *beta) + p.y;
temp1 =repmat(temp,1,p.nbeta);
temp2 = temp1 .* p.X;
B = (temp2' * temp2)/p.nsample;
g = mean(temp2);
end
