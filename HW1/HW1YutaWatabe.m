%% Homework 1 for Empirical Methods
% * By Yuta Watabe
% * Created in 09/02/16
%%
diary('HW1YutaWatabe.txt')

%% Homework 1.1
%Create X
X = [1,1.5,3.5,4.78,5,7.2,9,10,10.1,10.56];
%Generate Y1, and Y2
Y1 = -2 + (0.5).*X;
Y2 = -2 + (0.5).*(X.^2);
%Plot Y1 and Y2 against X
plot(X,Y1,X,Y2);
legend('Y1','Y2');

%% Homework 1.2
%Create X
X = linspace(-40,60,120);
%Calculate the sum of X
sumX = sum(X);
display(sumX);

%% Homework 1.3
%Create A and b
A = [3 4 5; 2 12 6; 1 7 4];
b = [3;-2;10];

%Calculate C,D,E,F,x
C = A'*b;
D = inv(A'*A)*b;
E=0;
for i = 1:3
    for j =1:3
        E= E+ A(i,j)*b(i);
    end
end
F = [A(1,1:2);A(3,1:2)];
x = inv(A)*b;

%% Homework 1.4
%Create B
B = kron(eye(5),A); 

%% Homework 1.5
%Create A
rng(1);
A = normrnd(6.5,3,5,3);
%Create B
B= (A>=4);
%Display A and B
display(A);
display(B);
%% Homework 1.6
%Import data
data=readtable('datahw1.csv');

%Take care of the missing data
dataf = ismissing(data);
datau = data(~any(dataf,2),:);

%Convert table to data
datau = table2array(datau);

Y = datau(:,5);
X = [ones(size(Y,1),1) datau(:,3) datau(:,4) datau(:,6)];  
beta = (inv(X'*X)*X'*Y)';
display(beta);
diary('off');
