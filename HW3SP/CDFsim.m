%% This function uses CDF and give you back number according to the simulated shock
function [nstate] = CDFsim(draw,state,CDF)
K = size(CDF,2);
for k = 1:K
    if draw < CDF(state,k)
        nstate = k;
        break
    end
end

end