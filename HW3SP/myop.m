%% This function is to calculate myopic solution
function [dif] = myop(pnew,pold)
global cmat
[junk1,D1,junk2] = Dfunc(pnew,pold);
dif = 1- (1-D1).*(pnew-cmat);
end
