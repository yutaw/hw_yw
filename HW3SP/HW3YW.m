%% This file calculate the MPE for Homework
clear;
global L rho kappa l v delta beta cmat Prob

%% Setting parameters
L = 30;
rho = 0.85;
kappa = 10;
l = 15;
v = 10;
delta = 0.03;
beta = 1/(1.05);
eta = log(rho)/log(2);


% Setting transition matrix
Pnp = zeros(L);
Pp = zeros(L);

%Setting first three elements
Pnp(1,1) = 1;
Pp(1,1) = 1-(1-delta);
Pp(1,2) = (1-delta);
%Setting other elements
for j = 2:L
    Pnp(j,j) = (1-delta)^j; %If not produced
    Pnp(j,j-1,1) = 1-(1-delta)^j;
    if j < L
        Pp(j,j) = 1-(1-delta )^j; %If produced
        Pp(j,j+1) = (1-delta)^j;
    elseif j == L
        Pp(j,j) = 1;
    end     
end

%Setting kronecker. This has (onega1,omega2) = (1,1), (2,1), (3,1)... kind of matrix
Prob = zeros(L*L,L*L,3);
Prob(:,:,1) = kron(Pnp,Pnp);
Prob(:,:,2) = kron(Pnp,Pp);
Prob(:,:,3) = kron(Pp,Pnp);

%Setting cost
cost = zeros(L,1);
for j = 1:L
    if j <l
        cost(j,1) = kappa * j^eta;
    end
    if j >= l
        cost(j,1) = kappa * l^eta;
    end
end
cmat = repmat(cost,1,L);

%% VFI/Iteration

%Initial guess
p0 = ones(L)*15;
myfunmyop = @(pnew) myop(pnew,p0); %Solve myopic problem
p1 = fsolve(myfunmyop,p0);
[D0,D1,D2] = Dfunc(p1,p1);
V1= D1.*(p1-cmat);
V1 = reshape(V1,L*L,1);

%Set parameters
Maxiter = 2000;
tol = 1e-5;

p = p1;
V = V1;
W10 = reshape(Prob(:,:,1)*V,L,L); 
W11 = reshape(Prob(:,:,2)*V,L,L);
W12 = reshape(Prob(:,:,3)*V,L,L);

iter = 0;
dif =1;

while dif > tol
    iter = iter+1
      if iter > Maxiter
          break
      end
    Vold = V;
    pold = p;
    p = fsolve(@(pnew) FOC(pnew,pold,W10,W11,W12),pold);
    [D0,D1,D2] = Dfunc(p,pold);
    V = D1.*(p-cmat) + beta * (D0 .* W10 + D1 .* W11 + D2 .* W12);
    V = reshape(V,L*L,1);
    W10 = reshape(Prob(:,:,1)*V,L,L); 
    W11 = reshape(Prob(:,:,2)*V,L,L);
    W12 = reshape(Prob(:,:,3)*V,L,L);
    vdif = max(abs((V-Vold)./(1+V)));
    pdif =max(max(abs((p-pold)./(1+p))));
    dif = max(pdif,vdif);
end

Vre = reshape(V,L,L);

figure(1);
mesh(p);
saveas(gcf,'Fig1.png');
figure(2);
mesh(Vre);
saveas(gcf,'Fig2.png');

save policy.mat p
save V.mat Vre 
%% Simulation

%Load policy function
load ('policy.mat');
policy = p;  %Rename policy function policy

%Setting parameters
N= 10000;
T=300;

%Generating uniform distribution to simulate random sales
rng(10);
sale = rand(T,N); 
depr = rand(2,T,N); %Utilize three-dimesntion. (player,time,simulation)

%Generate CDF for transition matrix (of quality)
PnpCDF = cumsum(Pnp,2);
PpCDF = cumsum(Pp,2);

%Setting boxes
qsim = ones(2,T,N)*999; %If error, there will be 999
psim = ones(2,T,N)*999;
ssim = ones(T,N)*999;

qsim(:,1,:)=ones(2,1,N); %Simulation of quality staring from (1,1)

for t = 1:T
for n = 1:N
    psim(1,t,n) = policy(qsim(1,t,n),qsim(2,t,n));
    psim(2,t,n) = policy(qsim(2,t,n),qsim(1,t,n));
    sprob = Dprob(psim(1,t,n),psim(2,t,n));
    ssim(t,n) = CDFsim(sale(t,n),1,sprob);
 if ssim(t,n) == 1
     qsim(1,t+1,n) = CDFsim(depr(1,t,n),qsim(1,t,n),PnpCDF);
     qsim(2,t+1,n) = CDFsim(depr(2,t,n),qsim(2,t,n),PnpCDF);
 elseif ssim(t,n) == 2
     qsim(1,t+1,n) = CDFsim(depr(1,t,n),qsim(1,t,n),PpCDF);
     qsim(2,t+1,n) = CDFsim(depr(2,t,n),qsim(2,t,n),PnpCDF);
 elseif ssim(t,n) == 3
     qsim(1,t+1,n) = CDFsim(depr(1,t,n),qsim(1,t,n),PnpCDF);
     qsim(2,t+1,n) = CDFsim(depr(2,t,n),qsim(2,t,n),PpCDF);
 end
end
end

%% Calculating distribution
Dist10 = zeros(L);
for n = 1:N
   Dist10(qsim(1,10,n),qsim(2,10,n)) = Dist10(qsim(1,10,n),qsim(2,10,n)) + 1;
end

Dist20 = zeros(L);
for n = 1:N
   Dist20(qsim(1,20,n),qsim(2,20,n)) = Dist10(qsim(1,20,n),qsim(2,20,n)) + 1;
end

Dist30 = zeros(L);
for n = 1:N
   Dist30(qsim(1,30,n),qsim(2,30,n)) = Dist30(qsim(1,30,n),qsim(2,30,n)) + 1;
end

Dist10 = Dist10/N;
Dist20 = Dist20/N;
Dist30 = Dist30/N;

figure(3);
mesh(Dist10);
saveas(gcf,'Fig3.png');
figure(4);
mesh(Dist20);
saveas(gcf,'Fig4.png');
figure(5);
mesh(Dist30);
saveas(gcf,'Fig5.png');
%% Calculating stationary distribution

t=1; %Start from initial period

%Load policy function
load ('policy.mat');
policy = p;  %Rename policy function policy

disttol = 1e-3*2;
distdif = 1;
T = 1000; %Max iteration
N = 10000; %Number of simulation

%Generating uniform distribution to simulate random sales
rng(10);
sale = rand(T,N); 
depr = rand(2,T,N); %Utilize three-dimesntion. (player,time,simulation)

%Generate CDF for transition matrix (of quality)
PnpCDF = cumsum(Pnp,2);
PpCDF = cumsum(Pp,2);

%Setting boxes
Stddist = zeros(L,L,T+1); %Distribution
qsim = ones(2,T,N)*999; %Box of quality
qsim(:,1,:)=ones(2,1,N); %Box of quality staring from (1,1)
psim = ones(2,T,N)*999; %Box of prices
ssim = ones(T,N)*999; %Box of sales


while distdif > disttol
    if t > T-1
        break
    end
    display(t);
    for n = 1:N
    psim(1,t,n) = policy(qsim(1,t,n),qsim(2,t,n));
    psim(2,t,n) = policy(qsim(2,t,n),qsim(1,t,n));
    sprob = Dprob(psim(1,t,n),psim(2,t,n));
    ssim(t,n) = CDFsim(sale(t,n),1,sprob);
  if ssim(t,n) == 1
      qsim(1,t+1,n) = CDFsim(depr(1,t,n),qsim(1,t,n),PnpCDF);
      qsim(2,t+1,n) = CDFsim(depr(2,t,n),qsim(2,t,n),PnpCDF);
  elseif ssim(t,n) == 2
      qsim(1,t+1,n) = CDFsim(depr(1,t,n),qsim(1,t,n),PpCDF);
      qsim(2,t+1,n) = CDFsim(depr(2,t,n),qsim(2,t,n),PnpCDF);
  elseif ssim(t,n) == 3
      qsim(1,t+1,n) = CDFsim(depr(1,t,n),qsim(1,t,n),PnpCDF);
      qsim(2,t+1,n) = CDFsim(depr(2,t,n),qsim(2,t,n),PpCDF);
  end
    Stddist(qsim(1,t,n),qsim(2,t,n),t) = Stddist(qsim(1,t,n),qsim(2,t,n),t) + 1;
    end
    if t >1
    distdif = max(max(abs(Stddist(:,:,t) - Stddist(:,:,t-1))))/N;
    display(distdif);
    end
    t = t+1;
end

figure(6);
mesh(Stddist(:,:,t-1));
saveas(gcf,'Fig6.png');

%% Think of analytically solving steady state