%% This function is to calculate FOC
function [dif] = FOC(pnew,pold,W10,W11,W12)
global beta cmat
[D0,D1,D2] = Dfunc(pnew,pold);
dif = 1- (1-D1).*(pnew-cmat) - beta*W11 + beta*(D0.*W10 + D1.*W11 + D2.*W12);
end