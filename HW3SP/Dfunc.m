%% This function is to calculate D_n(p_n,p_(-n)(\omega))
function [D0,D1,D2] = Dfunc(pnew,pold)
global v
D0 = 1./(1+exp(v-pold')+exp(v-pnew));
D1 = exp(v-pnew)./(1+ exp(v-pold')+exp(v-pnew));
D2 = exp(v-pold')./(1+exp(v-pold')+exp(v-pnew));
end
