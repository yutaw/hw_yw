%% This function is to calculate sales probability given prices
function [sprob] = Dprob(p,po)
global v
sprob(1) = 1./(1+exp(v-p)+exp(v-po));
sprob(2) = sprob(1) + exp(v-p)./(1+ exp(v-p)+exp(v-po));
sprob(3) = sprob(2) + exp(v-po)./(1+exp(v-p)+exp(v-po));
end