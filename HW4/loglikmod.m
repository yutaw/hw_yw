%This function derives the log-likelihood function for each nord and
%taking weighted sum.

function [loglikmod]= loglikmod(nords,gamma,data)
nnord = length(nords.betas);
for i = 1:nnord
   betax = nords.betas(i) * data.X + gamma * data.Z;
   prob = (1+exp(-betax)).^(-1);
   likNT(i,:) = prod(prob.*data.Y + (1-data.Y).*(1-prob),1).*nords.weights(i);
end
likind =sum(likNT,1);
loglikmod = sum(log(likind));
end