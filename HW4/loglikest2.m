%This function estimates the loglikelihood function using simulation

function [loglikestimates2]= loglikest2(param,data)

beta = param(1);
u = param(2);
sigma_beta =param(3);
sigma_betau = param(2);
% comment from TA: you use the same number for sigma_betau and u
gamma = param(4);


cor = [sigma_beta,sigma_betau;sigma_betau,u]; %Make correlation matrix
chcor = chol(cor); %Cholesky decomposition

rng(3);
randoms = randn(100,2)*chcor;
betas = randoms(:,1)+beta;
us = randoms(:,2) +u;
weights = ones(100,1)./20;

loglikestimates2 = -1*loglik2(betas,us,weights,gamma,data);
end