%This indicates whether the matrix is positve definite or not

function[c,ceq] = psd(param)
cor = [param(3),param(2);param(2),param(2)]
c = -eig(cor);
ceq = [];
end