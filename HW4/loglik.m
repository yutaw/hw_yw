%This function derives the log-likelihood function for each nord and
%taking weighted sum.

function [loglik]= loglik(nords,data)
nnord = length(nords.betas);
for i = 1:nnord
   betax = nords.betas(i) * data.X;
   prob = (1+exp(-betax)).^(-1);
   likNT(i,:) = prod(prob.*data.Y + (1-data.Y).*(1-prob),1).*nords.weights(i);
end
likind =sum(likNT,1);
loglik = sum(log(likind));
end