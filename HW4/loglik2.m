%This function derives the log-likelihood function for each nord and
%taking weighted sum. I add u param for this.

function [loglik]= loglik2(betas,us,weights,gamma,data)
nnord = length(betas);
for i = 1:nnord
   util = betas(i) * data.X + us(i) + gamma * data.Z ;
   prob = (1+exp(-util)).^(-1);
   likNT(i,:) = prod(prob.*data.Y + (1-data.Y).*(1-prob),1).*weights(i);
end
likind =sum(likNT,1);
loglik = sum(log(likind));
end