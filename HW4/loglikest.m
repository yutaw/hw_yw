%This function estimates the loglikelihood function

function [loglikestimates]= loglikest(param,data,method)

beta = param(1);
sigma_beta =param(2);
gamma = param(3);

%Method 1 is with 5 nords
%Method 2 is with 10 nords
%Method 3 is with Monte-Carlo

if method ==1
   [nords.betas,nords.weights] = qnwnorm(5,beta,sigma_beta);
end
if method ==2
   [nords.betas,nords.weights] = qnwnorm(10,beta,sigma_beta);
end
if method ==3
    rng(2);
    nords.betas = randn(20,1).*sigma_beta+beta;
    nords.weights = ones(20,1)./20;
end

loglikestimates= -1*loglikmod(nords,gamma,data);
end