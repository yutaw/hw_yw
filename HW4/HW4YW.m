%Produced by Yuta Watabe

%% Setting

%Set diary
diary('HW4YutaWatabe.txt');

%Load compecon toolbox
cepath='C:\Users\Yuta\Git\compecon\'; path([cepath 'cetools;' cepath 'cedemos'],path);

%Load data
load('hw4data.mat')

%% Question 1

%Set parameters
beta0 = 0.1;
sigma_beta =1;

%Calculate the nord
[nord1.betas,nord1.weights] = qnwnorm(5,beta0,sigma_beta);

%Derive the log-likelihood
[loglik5]= loglik(nord1,data);
display(loglik5)

%% Question 2

%Calculate the nord
[nord2.betas,nord2.weights] = qnwnorm(10,beta0,sigma_beta);

%Derive the log-likelihood
[loglik10]= loglik(nord2,data);
display(loglik10)

%% Question 3
 % Derive the log-likelihood

rng(1);
nord3.betas = randn(20,1)+beta0;
nord3.weights = ones(20,1)./20;
[loglikmc]= loglik(nord3,data);

%% Question 4
loglikestq1 = @(param)loglikest(param,data,1);
loglikestq2 = @(param)loglikest(param,data,2);
loglikestq3 = @(param)loglikest(param,data,3);

%Paramters are beta sigma_beta and gamma
paramin = [0.1,1,0.1];
A = [];
b = [];
Aeq =[];
beq =[];
lb = [-100,0,-100];
ub = [100,100,100];

[est.nord5,fval.nord5] = fmincon(loglikestq1,paramin,A,b,Aeq,beq,lb,ub);
[est.nord10,fval.nord10] = fmincon(loglikestq2,paramin,A,b,Aeq,beq,lb,ub); 
[est.montecarlo,fval.montecarlo] = fmincon(loglikestq3,paramin,A,b,Aeq,beq,lb,ub); 

% Print the result
disp('We started the value from')
betain = paramin(1)
sigma_betain = paramin(2)
gamma = paramin(3)
disp('Result of beta, sigma_beta and gamma is')
est
disp('Resulting (negative) fval is')
fval

%% Question 5

%Parameters are beta, u, sigma_beta, gamma.
paramin = [1;0.5;1;0];
%This restriction implies the correlation matrix is positive definite, This
%is simply written as sigma_beta > sigma_u. This is because we need
%principle minor of the matrix to be positve. Giving both sigma_beta and
%sigma_u positive, all we need is sigma_u sigma_beta > sigma_u sigma_u.
A = [0,1,-1,0]; 
b = [0];
Aeq =[];
beq =[];
lb = [-100,0,0,-100];
ub = [100,100,100,100];

% comment from TA: you had mistake in sigma matrix in loglikest2.m 

loglikestq4 = @(param)loglikest2(param,data)

[est.q4,fval.q4] = fmincon(loglikestq4,paramin,A,b,Aeq,beq,lb,ub); 
disp('We started the value from')
betain = paramin(1)
uin = paramin(2)
sigma_betain = paramin(3)
gamma = paramin(4)
disp('Result of beta, u, sigma_beta, gamma is')
est.q4
disp('resuting (negative) fval is')
fval.q4

diary('off');
