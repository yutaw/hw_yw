%% Equilibrium function
% This investigates the equilibrium for HW2
function [x] = HW2eqmfunc(p,v)
%% Demand fucntion
u = v -p;
qdenom = 1+ sum(exp(u));
qnum = exp(u);
q = qnum ./ qdenom;
x = p - 1./(ones(3,1)-q);
end