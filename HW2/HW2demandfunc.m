%% Demand function
% This function derives the demand given p and v
function [q] = HW2eqmfunc(p,v)
%% Demand fucntion
u = v -p;
qdenom = 1+ sum(exp(u));
qnum = exp(u);
q = qnum ./ qdenom;
end