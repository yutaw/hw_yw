%% Homework2 by Yuta Watabe
% This is for HW2. Made by Yuta Watabe, in 09/18/2016

diary('HW2YutaWatabe.txt')

%% Question 1
% Given this assumption, for 0,i={A,B,C}
%
% $$q_{0}=\frac{1}{1+3(exp-2)}$$
%
% $$q_{i}=\frac{exp(-2)}{1+3(exp-2)}$$

%% Question 2
% Firm i will choose the price so the marginal revenue is zero (since
% marginal cost is zero), which implies $q_i + p_i * \frac{\partial
% q_A}{\partial p_A}  =0$. Given this demand structure, we have $q_i - q_i 
% (1-q_i)p_i = 0$. Now we have six equation for six unknown variables. We
% substitute demand function so that it will be three equations of p's.
% Define an equilibrium function, which is a function of prices, as
% HW2eqmfunc. I minimize this function using Broyden method.
% Before that, I define the initial guess 

%Setting optimization option
tol =  1.0e-12;
maxit = 200;
optset('broyden','maxit',maxit)
optset('broyden','tol',tol)

%Setting parameters
v = -1*ones(3,1);
p_1 = [1,1,1]';
p_2 = [0,0,0]';
p_3 = [0,1,2]';
p_4 = [3,2,1]';
p_in = [p_1 p_2 p_3 p_4];

%Setting boxes
eqnp = zeros(3,4,3);
eqnq = zeros(3,4,3);
focvresult =ones(4,3);
focresult = zeros(3,4,3);

tic
%Solving the foc through Broyden method
for i = 1:4
   [eqnp(:,i,1),focresult(:,i,1),flag,it,fjacinv] = broyden('HW2eqmfunc',p_in(:,i),v); 
   if flag == 1
   display('Did not converge')
   elseif flag == 2
   display('NaN generated')
   end
   eqnq(:,i,1) = HW2demandfunc(eqnp(:,i,1),v); 
   focvresult(i,1) = sum(focresult(:,i,1).^2);
end
calctime(1) = toc;


%% Question 3
% To use secant method, we need a second inital value. I set the second
% inital value as [0.5,0.5,0.5]. The result seems to work well. Different initial
% value derived similar results.

%Rewrite the parameter
p_0 = [0.5,0.5,0.5]';
p_in = [p_1 p_2 p_3 p_4];
v = -1*ones(3,1);


tic

for i = 1:4
it = 0;
focv= 1;
pold2 = p_0;
pold = p_in(:,i);
p = pold;
focold = HW2eqmfunc(pold,v);
focold2 = HW2eqmfunc(pold2,v);

while focv > tol
   it = it+1;
   if it > maxit
       display('Did not converge')
       break
   end
   for j = 1:3
   p(j) = pold(j) - (pold(j) - pold2(j))/(focold(j) - focold2(j))*focold(j);
   temp = HW2eqmfunc(p,v);
   focold2(j) = focold(j);
   focold(j) =temp(j);
   pold2(j) = pold(j);
   pold(j) = p(j);
   end
   foc = HW2eqmfunc(p,v);
   focv = sum(foc.^2,1);
end
focvresult(i,2) = focv;
focresult(:,i,2) =foc';
eqnp(:,i,2) = p;
eqnq(:,i,2) = HW2demandfunc(p,v);
end
calctime(2) = toc;

%% Question 4

%Set the parameter
p_in = [p_1 p_2 p_3 p_4];
v = -1*ones(3,1);

tic
for i = 1:4
it = 0;
focv = 1;
pold = p_in(:,i);
qold = HW2demandfunc(p_in(:,i),v);

while focv > tol
   it = it+1;
   if it > maxit
       display('Did not converge')
       break
   end
   p = 1./(ones(3,1) - qold);
   q = HW2demandfunc(p,v);
   foc(:,i) = 1 - (ones(3,1)-q) .* p; 
   focv = sum(foc(:,i).^2);
   pold = p;
   qold = HW2demandfunc(p,v);
end
focresult(:,i,3) = foc(:,i);
focvresult(i,3) = focv;
eqnp(:,i,3) = p;
eqnq(:,i,3) = q;
end
calctime(3) = toc;

%% Result review before Q5
% All result converged
eqnp(:,:,1)

%% 
% Convergence criteria is
focvresult(:,1)

%%
% Second method also converged for first two initial value. The estimate is
eqnp(:,:,2)

%% 
% Convergence criteria is
focvresult(:,2)

%%
% Third method also converged for all the value, which is 
display(eqnp(:,:,3))

%% 
% Convergence criteria is
focvresult(:,3)

%% 
% The speed of each prcedure is 
calctime

%%
% Third method converged faster than other two. This is probably because we
% are using best response function to search the equilibrium value. Boyden
% was slowest probably because calculating (approximating) was costly while
% not so useful compared to Gauss-Siedel. I would think that the profit
% maximization problem for each firms have relatively independent, so that
% Gauss-Siedel method is efficient enough.

%% Question 5
% comment from TA: you were supposed to put Va=Vb=-1, not 1. I changed it
% and it worked, it was minor mistake.
va = -1*ones(1,6);
vb = -1*ones(1,6);
vc = [-4,-3,-2,-1,0,1];
v = [va;vb;vc];

focq5 = zeros(3,6);
eqnpq5 = zeros(3,6);
eqnqq5 = zeros(3,6);

tic
for i = 1:6
pold = [1,1,1]';
qold = HW2demandfunc(pold,v(:,i));
it = 0;
focv = 1;
while focv > tol
   it = it+1;
   if it > maxit
       display('Did not converge')
       break
   end
   p = 1./(ones(3,1) - qold);
   q = HW2demandfunc(p,v(:,i));
   foc = 1 - (ones(3,1)-q) .* p; 
   focv = sum(foc.^2);
   pold = p;
   qold = HW2demandfunc(p,v(:,i));
end
focq5(:,i) = foc;
eqnpq5(:,i) = p;
eqnqq5(:,i) = q;
end
toc

plot(vc,eqnpq5(1,:),vc,eqnpq5(3,:))
legend('p_A','p_C');
diary('off');




   